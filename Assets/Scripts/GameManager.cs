﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class GameManager : MonoBehaviour
{
    //Gameobjects
    public GameObject cardGameObject;
    public CardController mainCardController;
    public SpriteRenderer cardSpriteRenderer;
    public RessourceManager ressourceManager;
   
    //Tweaking variables
    public float fMovingSpeed;
    public float fSideMargin;
    public float fSideTrigger;
    float alphaText;
    public Color textColor;
    //UI
    public TMP_Text characterDialogue;
    public TMP_Text actionQuote;
    //Card variables
    private string leftQuote;
    private string rightQuote;
    public Card currentCard;
    public Card testCard;

    public bool dragging = false;
    private Vector2 initialPose;
    private Vector2 posCard;

    private Vector2 iniPos;
    void Start()
    {
        LoadCard(testCard);
        

        iniPos = cardGameObject.transform.position;
    }
    void UpdateDialogue()
    {
        actionQuote.color = textColor;
        if (cardGameObject.transform.position.x > 0)
        {
            actionQuote.text = leftQuote;
        }
        else
        {
            actionQuote.text = rightQuote;
        }
    }
    void Update()
    {
        //Dialogue text
        textColor.a = Mathf.Min((Mathf.Abs(cardGameObject.transform.position.x - fSideMargin) / 0.5f), 1);
        if (cardGameObject.transform.position.x > fSideTrigger)
        {
            textColor.a = Mathf.Min(Mathf.Abs(cardGameObject.transform.position.x / 0.5f), 1);
            UpdateDialogue();
            if (Input.GetMouseButtonUp(0))
            {
                currentCard.Right();
                NewCard();
            }
        }
        //Le fondu du texte
        else if (cardGameObject.transform.position.x > fSideMargin)
        {
            textColor.a = Mathf.Min(Mathf.Abs(cardGameObject.transform.position.x / 0.5f), 1);
        }
        else if (cardGameObject.transform.position.x > -fSideMargin)
        { 
            textColor.a = Mathf.Min(Mathf.Abs(cardGameObject.transform.position.x / 0.5f), 1);
        }
        else if (cardGameObject.transform.position.x > -fSideTrigger)
        {
            textColor.a = Mathf.Min(Mathf.Abs(cardGameObject.transform.position.x / 0.5f), 1);
            UpdateDialogue();
        }
        else
        {
            if (Input.GetMouseButtonUp(0))
            {
                currentCard.Left();
                NewCard();
            }
        }

        UpdateDialogue();
        //Mouvement
        if (Input.GetMouseButtonDown(0))
        {
            dragging = true;
             initialPose = Camera.main.ScreenToWorldPoint(new Vector2( Input.mousePosition.x, 0));
             posCard = cardGameObject.transform.position;
        }
        else if (Input.GetMouseButtonUp(0))
        {
            dragging = false;
            cardGameObject.transform.position = iniPos;
        }

        if (dragging)
        {
            Vector2 pos = Camera.main.ScreenToWorldPoint(new Vector2(Input.mousePosition.x, 0));
            Vector2 move = pos - initialPose;
            cardGameObject.transform.position = posCard + move;
        }

        LoadCard(testCard);
    }
    void OnMouseUp()
    {
        //La carte va à gauche ou à droite
        if (!Input.GetMouseButton(0) && cardGameObject.transform.position.x > fSideTrigger)
        {
            currentCard.Right();
        }
        else if (!Input.GetMouseButton(0) && cardGameObject.transform.position.x > fSideTrigger)
        {
            currentCard.Left();
        }
    }

    public void LoadCard(Card card)
    {
        //Charge les infos de la carte
        leftQuote = card.leftQuote;
        rightQuote = card.rightQuote;
        currentCard = card;
        characterDialogue.text = card.dialogue;
    }

    public void NewCard()
    {
        GameObject.FindObjectOfType<DataBase>().NextText();
        
    }
}
