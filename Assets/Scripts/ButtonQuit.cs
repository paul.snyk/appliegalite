﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ButtonQuit : MonoBehaviour
{
    public GameObject panelQuit;
    public GameManager gameManager;
    public bool gameIsPaused;

    //Active l'écran de pause
    public void Update()
    {
        if (gameIsPaused)
        {
            gameManager.dragging = false;
        }
    }

    public void PausePanel()
    {
        Time.timeScale = 0;
        gameIsPaused = true;
    }

    //Retour au jeu
    public void ReturnGame()
    {
        Time.timeScale = 1;
        gameIsPaused = false;
    }

    //Retourner à l'écran principal
    public void ReturnMainMenu()
    {
        Time.timeScale = 1;
        SceneManager.LoadScene(0);
    }
}
